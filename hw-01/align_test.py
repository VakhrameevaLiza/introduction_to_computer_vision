#!/usr/bin/python

from sys import argv, stdout, exit
from os.path import basename
from glob import iglob
from align import align
import matplotlib.pyplot as plt
"""
if len(argv) != 3:
    stdout.write('Usage: %s input_dir output_dir\n' % argv[0])
    exit(1)

input_dir = argv[1]
output_dir = argv[2]
"""
input_dir = "big"
output_dir = "out"
for i, filename in enumerate(iglob(input_dir + '/*.png')):
    print(i)
    img = plt.imread(filename)
    img = align(img, i)
    plt.imsave(output_dir + '/' + basename(filename), img)
print("end")
