import matplotlib.image
import matplotlib.pyplot as plt
from scipy import misc as sm
import math
import numpy as np
import scipy.ndimage
from PIL import Image


def crop_images(images):
    for i in range(len(images)):
        lx, ly = images[i].shape
        images[i] = images[i][lx / 15: -lx / 15, ly / 15: -ly / 15]

    shapes = []
    for img in images:
        shapes.append(img.shape)
    if not shapes[0][0] == shapes[1][0] and shapes[1][0] == shapes[2][0]:
        x = min(shapes[0][0], shapes[1][0], shapes[2][0])
    else:
        x = shapes[0][0]
    if not shapes[0][1] == shapes[1][1] and shapes[1][1] == shapes[2][1]:
        x = min(shapes[0][1], shapes[1][1], shapes[2][1])
    else:
        y = shapes[0][1]
    same_images = []
    for img in images:
        same_images.append(img[0:x, 0:y])
    return same_images


def cut_image_into_three_parts(image):
    lx, ly = image.shape
    new_lx = lx / 3
    images = []
    for i in range(0, 3):
        new_image = image[i * new_lx: (i + 1) * new_lx, 0: ly]
        images.append(new_image)
    return images


def count(image1, image2, delta):
    shape1 = image1.shape
    shape2 = image2.shape
    coord1 = [0, 0, shape1[0] - 1, shape1[1] - 1]
    coord2 = [0 + delta[0], 0 + delta[1], shape2[0] - 1 + delta[0], shape2[1] - 1 + delta[1]]
    coord = find_overlapping_area(coord1, coord2)
    copy1 = image1[coord[0]:coord[2] + 1, coord[1]:coord[3] + 1]

    if delta[0] < 0:
        x1 = math.fabs(delta[0])
        x2 = shape2[0] - 1
    else:
        x1 = 0
        x2 = shape2[0] - 1 - math.fabs(delta[0])

    if delta[1] < 0:
        y1 = math.fabs(delta[1])
        y2 = shape2[1] - 1
    else:
        y1 = 0
        y2 = shape2[1] - 1 - math.fabs(delta[1])
    copy2 = image2[x1: x2 + 1, y1: y2 + 1]

    diff = copy2 - copy1
    mult = diff * diff
    sum = mult.sum()
    new_shape = copy1.shape
    sum /= new_shape[0] * new_shape[1]
    return sum


def find_first_shift(image1, image2):
    answer = [0, 0]
    min = 100000
    for i in range(-20, 20):
        for j in range(-20, 20):
            delta = [i, j]
            metric = count(image1, image2, delta)
            if metric < min:
                min = metric
                answer = delta
    return answer


def find_best_shift_appr(image1, image2, approximate_shift):
    answer = approximate_shift
    min = 100000
    for i in range(-5, 5):
        for j in range(-5, 5):
            delta = [approximate_shift[0] + i, approximate_shift[1] + j]
            metric = count(image1, image2, delta)
            if metric < min:
                min = metric
                answer = delta
    return answer


def find_overlapping_area(coord1, coord2):
    coord = [0] * 4
    coord[0] = max(coord1[0], coord2[0])
    coord[1] = max(coord1[1], coord2[1])
    coord[2] = min(coord1[2], coord2[2])
    coord[3] = min(coord1[3], coord2[3])
    return coord


def find_overlapping_area_(coord1, coord2, coord3):
    coord = [0] * 4
    coord[0] = max(coord1[0], coord2[0], coord3[0])
    coord[1] = max(coord1[1], coord2[1], coord3[1])
    coord[2] = min(coord1[2], coord2[2], coord3[2])
    coord[3] = min(coord1[3], coord2[3], coord3[3])
    return coord


def steps(big_canals):

    red = [big_canals[2]]
    green = [big_canals[1]]
    blue = [big_canals[0]]

    shape = red[0].shape
    count = 0

    while shape[0] > 500:
        small_red = scipy.ndimage.zoom(red[count], 0.5, order=0)
        red.append(small_red)

        small_green = scipy.ndimage.zoom(green[count], 0.5, order=0)
        green.append(small_green)

        small_blue = scipy.ndimage.zoom(blue[count], 0.5, order=0)
        blue.append(small_blue)

        shape = small_red.shape
        count += 1

    approximate_shift1 = find_first_shift(blue[count], green[count])
    approximate_shift2 = find_first_shift(blue[count], red[count])

    for i in range(1, count + 1):
        approximate_shift1[0] *= 2
        approximate_shift1[1] *= 2
        approximate_shift2[0] *= 2
        approximate_shift2[1] *= 2
        approximate_shift1 = find_best_shift_appr(blue[count - i], green[count - i], approximate_shift1)
        approximate_shift2 = find_best_shift_appr(blue[count - i], red[count - i], approximate_shift2)
    return approximate_shift1, approximate_shift2


def align(image, num):
    images = cut_image_into_three_parts(image)
    images = crop_images(images)
    delta1, delta2 = steps(images)

    shape = images[0].shape
    coord0 = [0, 0, shape[0], shape[1]]
    coord1 = [0 + delta1[0], 0 + delta1[1], shape[0] + delta1[0], shape[1] + delta1[1]]
    coord2 = [0 + delta2[0], 0 + delta2[1], shape[0] + delta2[0], shape[1] + delta2[1]]
    overlap = find_overlapping_area_(coord0, coord1, coord2)
    x = overlap[2] - overlap[0]
    y = overlap[3] - overlap[1]
    b = images[0][overlap[0]: overlap[2], overlap[1]: overlap[3]]
    g = images[1][overlap[0] - delta1[0]: overlap[2] - delta1[0], overlap[1] - delta1[1]: overlap[3] - delta1[1]]
    r = images[2][overlap[0] - delta2[0]: overlap[2] - delta2[0], overlap[1] - delta2[1]: overlap[3] - delta2[1]]
    rgbArray = np.dstack((r, g, b))
    return rgbArray

