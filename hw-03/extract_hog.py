import scipy.signal
import matplotlib.pyplot as plt
import numpy as np
import scipy.misc
import math


def count_gradient_angle(img):
    gray_img = img[:, :, 0] * 0.3 + img[:, :, 1] * 0.59 + img[:, :, 2] * 0.11
    gray_img *= 255
    operators = {"x": np.array([[0, 0, 0], [-1, 0, 1], [0, 0, 0]]), "y": np.array([[0, -1, 0], [0, 0, 0], [0, 1, 0]])}
    der = dict()
    der["x"] = scipy.signal.convolve(gray_img, operators["x"], mode='same')
    der["y"] = scipy.signal.convolve(gray_img, operators["y"], mode='same')
    gr = np.sqrt(der["x"] * der["x"] + der["y"] * der["y"])
    ang = np.arctan2(der["y"], der["x"])
    return gr, ang


def check_num_in_cell(ang):
    bin = np.pi / 4
    abs_ang = math.fabs(ang)
    if abs_ang <= bin:
        num = 0
    if 2 * bin >= abs_ang > bin:
        num = 1
    if 3 * bin >= abs_ang > 2 * bin:
        num = 2
    if 4 * bin >= abs_ang > 3 * bin:
        num = 3

    if ang >= 0:
        return num
    else:
        return num + 4


def make_cell(gr_slice, ang_slice):
    cell = np.zeros(8)
    for i in range(ang_slice.shape[0]):
        for j in range(ang_slice.shape[1]):
            num = check_num_in_cell(ang_slice[i][j])
            cell[num] += gr_slice[i][j]
    px_count = ang_slice.shape[0] * ang_slice.shape[1]
    cell /= px_count
    return cell


def make_cells_arr(gr, ang):
    cells_arr_shape = [10, 10]
    cells = np.zeros((10, 10, 8))
    mod = {"x": gr.shape[0] % 10, "y": gr.shape[1] % 10}
    cell_shape_in_px = [int(gr.shape[0] / cells_arr_shape[0]), int(gr.shape[1] / cells_arr_shape[1])]  # in pixels
    count = 0
    for i in range(0, cells_arr_shape[0]):
        for j in range(0, cells_arr_shape[1]):
            x1 = int(mod["x"] / 2) + i * cell_shape_in_px[0]
            x2 = x1 + cell_shape_in_px[0]
            y1 = int(mod["y"] / 2) + j * cell_shape_in_px[1]
            y2 = y1 + cell_shape_in_px[1]
            cells[i][j] = make_cell(gr[x1: x2, y1: y2], ang[x1: x2, y1: y2])
            count += 1
    return cells


def make_block(cells):
    block = np.zeros(cells.shape[0] * cells.shape[1] * cells.shape[2])
    count = 0
    for i in range(cells.shape[0]):
        for j in range(cells.shape[1]):
            for k in range(cells.shape[2]):
                block[count] = cells[i][j][k]
                count += 1
    eps = 10e-5
    block = block / np.sqrt(block * block + eps)
    return block


def make_arr_of_blocks(cells):
    blocks = np.zeros(((25 + 16), 2 * 2 * cells.shape[2]))
    count = 0

    for i in range(0, 9, 2):
        for j in range(0, 9, 2):
            block = make_block(cells[i: i + 2, j: j+2])
            blocks[count] = block
            count += 1

    for i in range(1, 8, 2):
        for j in range(1, 8, 2):
            block = make_block(cells[i: i + 2, j: j+2])
            blocks[count] = block
            count += 1
    return blocks


def make_vector(blocks):
    vector = np.zeros(blocks.shape[0] * blocks.shape[1])
    count = 0
    for block in blocks:
        len = block.shape[0]
        vector[count * len: (count + 1) * len] = block
        count += 1
    return vector


def extract_hog(img, roi):
    gr, ang = count_gradient_angle(img)
    cells = make_cells_arr(gr, ang)
    blocks = make_arr_of_blocks(cells)
    vector = make_vector(blocks)
    return vector