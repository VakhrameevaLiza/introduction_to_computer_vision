import numpy as np
import math
import matplotlib.pyplot as plt
import random
from skimage import img_as_ubyte
from skimage import data
from skimage.filters.rank import median
from skimage.morphology import disk
from skimage.filters.rank import mean_bilateral
from skimage.filters import threshold_adaptive


class DSU:
    def __init__(self, size):
        self.rank = [0] * size
        self.parent = [i for i in range(size)]

    def find(self, x):
        path = [x]
        if self.parent[x] != x:
            while self.parent[x] != x:
                x = self.parent[x]
                path.append(x)
            for elem in path:
                self.parent[elem] = x
        return x

    def union(self, x, y):
        x_root = self.find(x)
        y_root = self.find(y)

        if x_root == y_root:
            return

        if self.rank[x] < self.rank[y]:
            self.parent[x_root] = y_root
        elif self.rank[x] > self.rank[y]:
            self.parent[y_root] = x_root
        else:
            self.parent[y_root] = x_root
            self.rank[x] += 1


class Area:
    def __init__(self, x, y):
        self.x_start = x
        self.y_start = y
        self.x_end = x
        self.y_end = y

    def check(self, x, y):
        self.x_start = min(self.x_start, x)
        self.y_start = min(self.y_start, y)
        self.x_end = max(self.x_end, x)
        self.y_end = max(self.y_end, y)

    def get_area_coord(self):
        return self.x_start, self.y_start, self.x_end, self.y_end

    def get_square(self):
        return (self.x_end - self.x_start) * (self.y_end - self.y_start)

    def get_x(self):
        return self.x_end - self.x_start

    def get_y(self):
        return self.y_end - self.y_start


def generate_template(digit_dir_path):
    images = []
    max_x = 0
    max_y = 0
    for i in range(1, 101):
        base_name = digit_dir_path + "/"
        if i < 10:
            name = base_name + "00" + str(i) + ".bmp"
        elif 10 <= i < 100:
            name = base_name + "0" + str(i) + ".bmp"
        else:
            name = base_name + str(i) + ".bmp"
        plt.gray()
        img = plt.imread(name)
        bin = get_binary_image(img)
        shape = img.shape
        max_x = max(max_x, shape[0])
        max_y = max(max_y, shape[1])
        images.append(bin)

    template = np.zeros((max_x, max_y), dtype=np.float32)
    for img in images:
        shape = img.shape
        delta_x = (max_x - shape[0]) / 2
        delta_y = (max_y - shape[1]) / 2

        for i in range(shape[0]):
            for j in range(shape[1]):
                template[i + delta_x][j + delta_y] += img[i][j]
    template /= 100
    return template


def center(fix, move):
    coord = {}
    coord["move"] = [0] * 4
    coord["fix"] = [0] * 4

    if fix.shape[0] / 2 > move.shape[0] / 2:
        coord["fix"][0] = fix.shape[0] / 2 - move.shape[0] / 2
        coord["move"][0] = 0
        coord["fix"][2] = fix.shape[0] / 2 + move.shape[0] / 2
        coord["move"][2] = move.shape[0]
    else:
        coord["fix"][0] = 0
        coord["move"][0] = move.shape[0] / 2 - fix.shape[0] / 2
        coord["fix"][2] = fix.shape[0]
        coord["move"][2] = move.shape[0] / 2 + fix.shape[0] / 2

    if fix.shape[1] / 2 > move.shape[1] / 2:
        coord["fix"][1] = fix.shape[1] / 2 - move.shape[1] / 2
        coord["move"][1] = 0
        coord["fix"][3] = fix.shape[1] / 2 + move.shape[1] / 2
        coord["move"][3] = move.shape[1]
    else:
        coord["fix"][1] = 0
        coord["move"][1] = move.shape[1] / 2 - fix.shape[1] / 2
        coord["fix"][3] = fix.shape[1]
        coord["move"][3] = move.shape[1] / 2 + fix.shape[1] / 2

    for i in range(4):
        coord["fix"][i] = int(coord["fix"][i])
        coord["move"][i] = int(coord["move"][i])

    fix_copy = fix[coord["fix"][0]: coord["fix"][2], coord["fix"][1]: coord["fix"][3]]
    move_copy = move[coord["move"][0]: coord["move"][2], coord["move"][1]: coord["move"][3]]
    return fix_copy, move_copy


def count_metric(template, digit):
    intersection = center(template, digit)
    diff = intersection[0] - intersection[1]
    diff *= diff
    metric = diff.sum()
    metric /= diff.shape[0] * diff.shape[1]
    return metric


def get_digit(templates, digit):
    min_metric = 1000
    ans = -1
    count = 0
    for template in templates:
        metric = count_metric(template, digit)
        if metric < min_metric:
            ans = count
            min_metric = metric
        count += 1
    return ans


def labeling(img, arr):
    shape = img.shape
    dsu = DSU(shape[0] * shape[1])
    label = 0
    shape = img.shape
    x = shape[0]
    y = shape[1]
    if img[0][0] == 0:
        label = 1
        arr[0][0] = 1
    for i in range(1, x):
        if img[i][0] == 0:
            if img[i - 1][0] == 1:
                arr[i][0] = arr[i-1][0]
                dsu.union(i * shape[1], (i - 1) * shape[1])
            else:
                label += 1
                arr[i][0] = label
    for j in range(1, y):
        if img[0][j] == 1:
            if img[0][j-1] == 1:
                arr[0][j] = arr[0][j-1]
                dsu.union(j, j - 1)
            else:
                label += 1
                arr[0][j] = label

    for i in range(1, x):
        for j in range(1, y):
            if img[i][j] == 0:
                if arr[i-1][j] == 0 and arr[i][j - 1] == 0:
                    label += 1
                    arr[i][j] = label
                elif arr[i-1][j] == 0 and arr[i][j - 1] != 0:
                    arr[i][j] = arr[i][j-1]
                    dsu.union(i * shape[1] + j, i * shape[1] + j - 1)
                elif arr[i-1][j] != 0 and arr[i][j - 1] == 0:
                    arr[i][j] = arr[i - 1][j]
                    dsu.union(i * shape[1] + j, (i - 1) * shape[1] + j)
                elif arr[i - 1][j] != 0 and arr[i][j - 1] != 0:
                    if arr[i-1][j] == arr[i][j-1]:
                        arr[i][j] = arr[i-1][j]
                        dsu.union(i * shape[1] + j, (i - 1) * shape[1] + j)
                    else:
                        arr[i][j] = arr[i-1][j]
                        arr[i][j-1] = arr[i-1][j]
                        dsu.union(i * shape[1] + j, (i - 1) * shape[1] + j)
                        dsu.union(i * shape[1] + j, i * shape[1] + j - 1)
    return dsu


def get_binary_image(noisy_image):
    img_median = median(noisy_image, disk(1))
    bilat = mean_bilateral(img_median.astype(np.uint16), disk(100), s0=10, s1=10)
    binary_adaptive = threshold_adaptive(bilat, 25)
    return binary_adaptive


def color(bin_img, dsu):
    shape = bin_img.shape
    color_img = np.ones((shape[0], shape[1], 3), dtype=np.int)
    dict = {}
    area = {}
    for i in range(shape[0]):
        for j in range(shape[1]):
            if bin_img[i][j] == 0:
                number = i * shape[1] + j
                label = dsu.find(number)
                if dict.get(label) is None:
                    r = random.randint(0, 255)
                    g = random.randint(0, 255)
                    b = random.randint(0, 255)
                    rgb = [r, g, b]
                    dict[label] = rgb
                    color_img[i][j] = rgb
                    area[label] = Area(i, j)
                else:
                    color_img[i][j] = dict[label]
                    area[label].check(i, j)
            else:
                color_img[i][j] = [1, 1, 1]
    return color_img, area


def find_overlapping_area(coord1, coord2):
    #находим общую область 2 изображений
    coord = [0] * 4
    coord[0] = max(coord1[0], coord2[0])
    coord[1] = max(coord1[1], coord2[1])
    coord[2] = min(coord1[2], coord2[2])
    coord[3] = min(coord1[3], coord2[3])
    return coord


def check_size(area):
    x = area.get_x()
    y = area.get_y()
    if 15 <= x <= 30 and 10 <= y <= 20:
        return True
    else:
        return False


def recognize(img, digit_templates, num):
    bin_img = get_binary_image(img)
    shape = bin_img.shape
    arr = np.zeros((shape[0], shape[1]), dtype=np.int)
    dsu = labeling(bin_img, arr)
    color_img, areas = color(bin_img, dsu)
    keys = areas.keys()
    possible_number_areas = []
    for key in keys:
        if check_size(areas[key]):
            possible_number_areas.append(areas[key])
    possible_number_areas.sort(key=lambda area: area.y_start)
    answer = [0] * 3
    for i, area in enumerate(possible_number_areas[1:4]):
        coord = area.get_area_coord()
        digit_area = bin_img[coord[0]: coord[2] + 1, coord[1]: coord[3] + 1]
        digit = get_digit(digit_templates, digit_area)
        answer[i] = digit
    return answer[0], answer[1], answer[2]
